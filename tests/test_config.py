from pathlib import Path
import pytest  # noqa: F401
import os
from unittest.mock import Mock, patch

from bytardag.config import build_database_url

def test_build_database_url_default():
    url = build_database_url()

    assert url == "sqlite:///./sql_app.db"


@patch.dict(os.environ, {"SQLALCHEMY_DATABASE_URL": "psychopg://user:pass@db.server/database"}, clear=True)
def test_build_database_url_only_sqlalchemy_database_url():
    url = build_database_url()

    assert url == "psychopg://user:pass@db.server/database"


@patch.dict(os.environ, {"SQLALCHEMY_DATABASE_URL": "psychopg://onlyuser@db.server/database", "DATABASE_PASS_FILE": "/path/to/file"}, clear=True)
@patch.object(Path, 'read_text')
@patch.object(Path, 'is_file')
def test_build_database_url_with_database_pass_file(mock_is_file, mock_read_text):
    mock_is_file.return_value = True
    mock_read_text.return_value = 'test_password'

    url = build_database_url()

    assert url == "psychopg://onlyuser:test_password@db.server/database"

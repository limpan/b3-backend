import logging
import os
from pathlib import Path
from dotenv import load_dotenv

logger = logging.getLogger()

load_dotenv()


def build_database_url():
    url = os.environ.get('SQLALCHEMY_DATABASE_URL')

    if url is None:
        return  "sqlite:///./sql_app.db"

    if os.environ.get('DATABASE_PASS_FILE'):
        pass_file_path = Path(os.environ.get('DATABASE_PASS_FILE'))

        if pass_file_path.is_file():
            password = pass_file_path.read_text()
            at_pos = url.find('@')
            url = f"{url[:at_pos]}:{password}{url[at_pos:]}"

    return url

class Config(object):
    AUTH0_AUDIENCE = os.environ.get('AUTH0_AUDIENCE')
    AUTH0_DOMAIN = os.environ.get('AUTH0_DOMAIN')
    CLIENT_ORIGIN_URL = os.environ.get('CLIENT_ORIGIN_URL') or 'http://localhost:4200'
    PORT = os.environ.get('PORT') or 8000

    SQLALCHEMY_DATABASE_URL = build_database_url()
